<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    protected $fillable = ['code', 'name'];

    public function alternative_ratings(){
        return $this->hasMany("\App\Model\AlternativeRating");
    }
}
