<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CriteriaRating extends Model
{
    public $timestamps = false;

    protected $fillable = ['criteria_id', 'name', 'score'];
}
