<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlternativeRating extends Model
{
    public $timestamps = false;

    protected $fillable = ['alternative_id', 'criteria_rating_id', 'criteria_id'];

    public function criteria_rating(){
        return $this->belongsTo("\App\Model\CriteriaRating");
    }
}
