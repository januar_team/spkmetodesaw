<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Criteria extends Model
{
    public $timestamps = false;

    protected $fillable = ['code', 'name', 'weight', 'description'];

    public function ratings(){
        return $this->hasMany("\App\Model\CriteriaRating", "criteria_id");
    }
}
