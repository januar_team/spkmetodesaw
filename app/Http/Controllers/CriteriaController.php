<?php

namespace App\Http\Controllers;

use App\Model\Criteria;
use App\Model\CriteriaRating;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;

class CriteriaController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $data = Criteria::select('*');
            return DataTables::of($data)->make(true);
        }
        return $this->view();
    }

    public function add(Request $request){
        if ($request->isMethod('post')){
            $this->validate($request,[
                'name' => 'required',
                'weight' => 'required|numeric|min:0|max:1',
                'description' => 'required',
                'code'  =>'required|unique:criterias',
            ]);

            $criteria = new Criteria();
            $criteria->fill($request->all());
            $criteria->save();

            return redirect()->route('criteria');
        }
        return $this->view();
    }

    public function edit(Request $request, $id){
        $criteria = Criteria::find($request->id);

        if (!$criteria){
            return redirect()->route('criteria');
        }

        if ($request->isMethod('post')){
            $this->validate($request,[
                'name'          => 'required',
                'weight'        => 'required|numeric|min:0|max:1',
                'description'   => 'required',
                'code'          => [
                    'required',
                    Rule::unique('criterias')->ignore($request->id)
                ]
            ]);

            $criteria->fill($request->all());
            $criteria->save();

            return redirect()->route('criteria');
        }
        return $this->view(['criteria' => $criteria]);
    }

    public function delete(Request $request){
        $this->validate($request, [
            'id' => 'required',
        ]);

        $criteria = Criteria::find($request->id);
        $criteria->delete();

        return response()->json([
            'status' => true
        ]);
    }

    public function rating(Request $request, $id){
        if ($request->isMethod('post')){
            $data = CriteriaRating::where('criteria_id', '=', $id)->select('*');

            return DataTables::of($data)->make(true);
        }

        $criteria = Criteria::find($id);
        return $this->view(['criteria' => $criteria]);
    }

    public function rating_add(Request $request){
        $this->validate($request, [
            'criteria_id'   => 'required',
            'name'          => 'required',
            'score'         => 'required|numeric'
        ]);

        $rating = new CriteriaRating();
        $rating->fill($request->all());
        $rating->save();

        return response()->json(['status' => true]);
    }

    public function rating_edit(Request $request){
        $this->validate($request, [
            'id'            => 'required',
            'criteria_id'   => 'required',
            'name'          => 'required',
            'score'         => 'required|numeric'
        ]);

        $rating = CriteriaRating::find($request->id);
        $rating->fill($request->all());
        $rating->save();

        return response()->json(['status' => true]);
    }

    public function rating_delete(Request $request){
        $this->validate($request, [
            'id' => 'required',
        ]);

        try{
            $rating = CriteriaRating::find($request->id);
            $rating->delete();
        }catch (\Exception $ex){
            return response()->json([
                'status' => false,
                'message' => 'Data tidak dapat dihapus! Terdapat siswa dikelas ini.'
            ], 500);
        }

        return response()->json([
            'status' => true
        ]);
    }
}
