<?php

namespace App\Http\Controllers;

use App\Model\Alternative;
use App\Model\AlternativeRating;
use App\Model\Criteria;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;

class AlternativeController extends Controller
{
    public function index(Request $request){
        if ($request->isMethod('post')){
            $data = Alternative::select('*');
            return DataTables::of($data)->make(true);
        }
        return $this->view();
    }

    public function add(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'code'  =>'required|unique:alternatives',
        ]);

        $alternative = new Alternative();
        $alternative->fill($request->all());
        $alternative->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function edit(Request $request){
        $this->validate($request,[
            'id'    => 'required',
            'name' => 'required',
            'code'  =>[
                'required',
                Rule::unique('alternatives')->ignore($request->id)
            ]
        ]);

        $alternative = Alternative::find($request->id);
        $alternative->fill($request->all());
        $alternative->save();

        return response()->json([
            'status' => true
        ]);
    }

    public function delete(Request $request){
        $this->validate($request,[
            'id'    => 'required',
        ]);

        $alternative = Alternative::find($request->id);
        $alternative->delete();

        return response()->json([
            'status' => true
        ]);
    }

    public function rating(Request $request, $id){
        $alternative = Alternative::with('alternative_ratings')->find($id);
        if (!$alternative){
            return redirect()->route('alternative');
        }
        $alternative_rating = $alternative->alternative_ratings->keyBy('criteria_id');

        $criterias = Criteria::with('ratings')
            ->get();

        if ($request->isMethod('post')){
            $validation = [];
            foreach ($criterias as $criteria){
                $validation[strtolower($criteria->code)] = 'required';
            }

            $this->validate($request,$validation);

            foreach ($criterias as $criteria){
                AlternativeRating::updateOrCreate(
                    [
                        'alternative_id'    => $id,
                        'criteria_id'       => $criteria->id
                    ],
                    [
                        'criteria_rating_id' => $request->{strtolower($criteria->code)}
                    ]
                );
            }

            return redirect()->route('alternative');
        }

        return $this->view([
            'alternative' => $alternative,
            'alternative_ratings'   => $alternative_rating,
            'criterias' => $criterias
        ]);
    }
}
