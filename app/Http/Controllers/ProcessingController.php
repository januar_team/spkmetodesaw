<?php

namespace App\Http\Controllers;

use App\Model\Alternative;
use App\Model\Criteria;
use App\Model\CriteriaRating;
use Illuminate\Http\Request;

class ProcessingController extends Controller
{
    public function index(Request $request){
        $alternatives = Alternative::with('alternative_ratings')->get();
        $criterias = Criteria::get();

        $max_criteria = [];
        foreach ($criterias as $criteria){
            $max_criteria[$criteria->id] = CriteriaRating::where('criteria_id', '=', $criteria->id)->max('score');
        }

        $normalization = [];
        foreach ($alternatives as $al){
            $alternative_ratings = $al->alternative_ratings->keyBy('criteria_id');
            $item = [];
            foreach ($criterias as $criteria){
                $item[$criteria->id] = isset($alternative_ratings[$criteria->id]) ? $alternative_ratings[$criteria->id]->criteria_rating->score / $max_criteria[$criteria->id] : 0;
            }
            $normalization[$al->id] = $item;
        }

        $rank_result = [];
        foreach ($alternatives as $al){
            $item = [];
            $item['result'] = 0;
            $item['rank'] = count($alternatives);
            foreach ($criterias as $criteria){
                $item[$criteria->id] = $normalization[$al->id][$criteria->id] * $criteria->weight;
                $item['result'] += $item[$criteria->id];
            }
            $rank_result[$al->id] = $item;
        }

        $keys = array_keys($rank_result);
        for ($i = 0; $i < count($keys) - 1; $i++){
            for ($j = $i + 1; $j < count($keys); $j++){
                if ($rank_result[$keys[$i]] > $rank_result[$keys[$j]]){
                    $rank_result[$keys[$i]]['rank']--;
                }else{
                    $rank_result[$keys[$j]]['rank']--;
                }
            }
        }

        $result = new \stdClass();
        foreach ($rank_result as $key => $value){
            if ($value['rank'] == 1){
                $result->alternative = Alternative::find($key);
                $result->value = $value['result'];
            }
        }

        return $this->view([
            'alternatives'  => $alternatives,
            'criterias'     => $criterias,
            'normalization' => $normalization,
            'rank_result'   => $rank_result,
            'result'        => $result
        ]);
    }
}
