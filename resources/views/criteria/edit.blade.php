@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <form action="{{route('criteria.edit', ['id' => $criteria->id])}}" method="post" class="form-horizontal" autocomplete="off">
                    @csrf
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Edit Criteria
                    </div>
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Code</label>
                            <div class="col-md-9">
                                <input type="hidden" name="id" value="{{$criteria->id}}">
                                <input class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}"
                                       name="code" value="{{ old('code', $criteria->code) }}" type="text"
                                       placeholder="Code">
                                @if ($errors->has('code'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Name</label>
                            <div class="col-md-9">
                                <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                       name="name" value="{{ old('name', $criteria->name) }}" type="text"
                                       placeholder="Name">
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Weight</label>
                            <div class="col-md-3 col-sm-12">
                                <input class="form-control{{ $errors->has('weight') ? ' is-invalid' : '' }}"
                                       name="weight" value="{{ old('weight', $criteria->weight) }}" type="number" min="0" step=".01"
                                       placeholder="Weight">
                                @if ($errors->has('weight'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('weight') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label">Description</label>
                            <div class="col-md-9">
                                <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                       name="description" placeholder="Description">{{ old('description', $criteria->description) }}</textarea>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('isbn') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-save"></i> Save
                        </button>
                        <a href="{{route('criteria')}}" class="btn btn-sm btn-danger">
                            <i class="fa fa-arrow-left"></i> Back
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
