@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Alternative
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered">
                        <thead>
                        <tr>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Code</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Name</th>
                            <th colspan="{{count($criterias)}}" style="text-align: center">Criteria</th>
                        </tr>
                        <tr>
                            @foreach($criterias as $criteria)
                                <th style="text-align: center; vertical-align: middle">
                                    {{sprintf("%s (%.2f)", $criteria->code, $criteria->weight)}}
                                    <br>
                                    <span style="font-size: 12px">{{$criteria->name}}</span>
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alternatives as  $alternative)
                            <?php
                            $alternative_ratings = $alternative->alternative_ratings->keyBy('criteria_id');
                            ?>
                            <tr>
                                <td>
                                    {{$alternative->code}}
                                </td>
                                <td>
                                    {{$alternative->name}}
                                </td>
                                @foreach($criterias as $criteria)
                                    <td style="text-align: center">
                                        {{(isset($alternative_ratings[$criteria->id])) ? $alternative_ratings[$criteria->id]->criteria_rating->score : ""}}
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Normalization
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered">
                        <thead>
                        <tr>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Code</th>
                            <th colspan="{{count($criterias)}}" style="text-align: center">Criteria</th>
                        </tr>
                        <tr>
                            @foreach($criterias as $criteria)
                                <th style="text-align: center; vertical-align: middle">
                                    {{$criteria->code}}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alternatives as  $alternative)
                            <tr>
                                <td>
                                    {{$alternative->code}}
                                </td>
                                @foreach($criterias as $criteria)
                                    <td style="text-align: center">
                                        {{$normalization[$alternative->id][$criteria->id]}}
                                    </td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Rank Result
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm table-bordered">
                        <thead>
                        <tr>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Code</th>
                            <th colspan="{{count($criterias)}}" style="text-align: center">Criteria</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Result</th>
                            <th rowspan="2" style="text-align: center; vertical-align: middle">Rank</th>
                        </tr>
                        <tr>
                            @foreach($criterias as $criteria)
                                <th style="text-align: center; vertical-align: middle">
                                    {{$criteria->code}}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($alternatives as  $alternative)
                            <tr>
                                <td>
                                    {{$alternative->code}}
                                </td>
                                @foreach($criterias as $criteria)
                                    <td style="text-align: center">
                                        {{$rank_result[$alternative->id][$criteria->id]}}
                                    </td>
                                @endforeach
                                <td>
                                    {{$rank_result[$alternative->id]['result']}}
                                </td>
                                <td>
                                    {{$rank_result[$alternative->id]['rank']}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    Keputusan terbaik yang terpilih adalah alternatif <span class="h3">{{sprintf("%s - %s", $result->alternative->code, $result->alternative->name)}}</span>
                    dengan nilai <span class="h3">{{$result->value}}</span>
                </div>
            </div>
        </div>
    </div>
        @endsection

        @section('script')
            <script>
                jQuery(document).ready(function ($) {

                });
            </script>
@endsection
