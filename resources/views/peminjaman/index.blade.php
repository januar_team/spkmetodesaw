@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Peminjaman

                    <div class="card-header-actions">
                        <a class="btn-add btn btn-primary btn-sm" href="{{route('rent.add')}}">
                            <i class="icon-plus"></i> Tambah peminjaman
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table id="tbl-book" class="table table-striped table-bordered datatable dataTable no-footer">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Judul Buku</th>
                            <th>Pengarang</th>
                            <th>Peminjam</th>
                            <th>NIM</th>
                            <th>Tanggal Peminjaman</th>
                            <th>Jatuh Tempo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-delete" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form class="form-horizontal" action="{{route('rent.delete')}}">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" id="id">
                        <P>Apakah Anda ingin membatalkan peminjaman ini?</P>
                        <div id="error_message" class="form-group"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-primary">YA</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        jQuery(document).ready(function ($) {
            var table = $('#tbl-book').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('rent') }}",
                    "type": "POST",
                },
                columns: [
                    {data: null, "orderable": false, searchable: "false",
//                        render: function (data, type, row, meta) {
//                            return meta.row + 1;
//                        }
                    },
                    {data: 'book.title', name: 'books.title'},
                    {data: 'book.author', name: 'book.author'},
                    {data: 'student.name', name: 'student.name'},
                    {data: 'student.nim', name: 'student.nim'},
                    {data: 'date', searchable: "false",
                        render: function (data, type, row, meta) {
                            return moment(data, "YYYY-MM-DD").format("DD MMM YY");
                        }
                    },
                    {data: 'expired_date', searchable: "false",
                        render: function (data, type, row, meta) {
                            return moment(data, "YYYY-MM-DD").format("DD MMM YY");
                        }
                    },
                    {
                        data: null, searchable: "false",
                        render: function (data, type, row, meta) {
                            var group = '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">';
                            group += '<a href="/return/add/' + data.id + '" data-toggle="tooltip" title="Pengembalian buku" ' +
                                'class="btn btn-brand btn-facebook"><i class="icon-arrow-right-circle"></i></a>';
                            group += '<a href="/rent/detail/' + data.id + '" data-toggle="tooltip" title="Detail peminjam" ' +
                                'class="btn btn-brand btn-vine" data-id="' + data.id + '">' +
                                '<i class="icons cui-people"></i></a>';
                            group += '<a href="#" data-toggle="tooltip" title="Batal peminjaman" ' +
                                'class="btn btn-brand btn-youtube btn-cancel" data-id="' + data.id + '">' +
                                '<i class="icons icon-ban"></i></a>';
                            group += '</div>';
                            return group;
                        },
                        "orderable": false,
                    }
                ],
                preDrawCallback: function(settings,json) {
                    if (settings.jqXHR != null)
                        settings.jqXHR.abort();
                },
                drawCallback: function (settings) {
                    $('[data-toggle="tooltip"]').tooltip();
                },
                "aoColumnDefs": [ {
                    "aTargets": [0],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        if (iCol === 0) {
                            $(nTd).html(iRow + 1);
                        }
                    }
                }],
            });

            $('#tbl-book').on('click', '.btn-cancel', function (event) {
                event.preventDefault();
                var id = $(this).data('id');
                $('#modal-delete form input#id').val(id);
                $('#modal-delete').modal();
            })

            $('#modal-delete form').submit(function (event) {
                event.preventDefault();
                var form = $(this);
                window.resetForm(form);
                var data = form.serialize();
                var submitBtn = form.find('button[type="submit"]');
                submitBtn.button_bs4('loading');
                $.ajax({
                    url: form.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    success: function (data, textStatus, request) {
                        if (data.status) {
                            $('#modal-delete').modal('toggle');
                            table.draw();
                        } else {
                            window.handleError(data, form);
                        }
                    },
                    error: function (response, XMLHttpRequest, textStatus, errorThrown) {
                        window.handleError(response, form);
                    },
                    complete : function(xhr){
                        submitBtn.button_bs4('reset');
                    }
                });
            });
        });
    </script>
@endsection
