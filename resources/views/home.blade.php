@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-lg-6">
            <div class="card text-white bg-primary">
                <div class="card-body pb-0">
                    <div class="text-value">{{\App\Model\Criteria::count('id')}}</div>
                    <div>Jumlah Criteria</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart1" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
        <div class="col-sm-6 col-lg-6">
            <div class="card text-white bg-info">
                <div class="card-body pb-0">
                    <div class="text-value">{{\App\Model\Alternative::count('id')}}</div>
                    <div>Jumlah Alternative</div>
                </div>
                <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
                    <canvas class="chart" id="card-chart2" height="70"></canvas>
                </div>
            </div>
        </div>
        <!-- /.col-->
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">Alternative List</h4>
                    <div class="small text-muted">{{\Carbon\Carbon::now()->format('d F Y')}}</div>
                </div>
            </div>

            <table class="table table-responsive-sm table-hover table-outline mb-0">
                <thead class="thead-light">
                <tr>
                    <th>Code</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                @foreach(\App\Model\Alternative::get() as $alternative)
                    <tr>
                        <td>{{$alternative->code}}</td>
                        <td>{{$alternative->name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{mix('js/Chart.js')}}"></script>
    <script src="{{asset('js/widgets.js')}}"></script>
@endsection
