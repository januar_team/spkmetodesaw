@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <form action="{{route('alternative.rating', ['id' => \Illuminate\Support\Facades\Request::segment(3)])}}" method="post" class="form-horizontal" autocomplete="off">
                    @csrf
                    <div class="card-header">
                        <i class="fa fa-align-justify"></i> Alternative Rating Value
                    </div>
                    <div class="card-body">
                        @foreach($criterias as $criteria)
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">{{$criteria->name . "($criteria->code)"}}</label>
                                <div class="col-md-8">
                                    <select class="form-control{{ $errors->has(strtolower($criteria->code)) ? ' is-invalid' : '' }}"
                                           name="{{strtolower($criteria->code)}}" >
                                        <option value="">Please Select</option>
                                        @foreach($criteria->ratings as $rating)
                                            <option value="{{$rating->id}}" {{(isset($alternative_ratings[$criteria->id]) && ($alternative_ratings[$criteria->id]->criteria_rating_id == $rating->id)) ? "selected":""}}>{{$rating->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has(strtolower($criteria->code)))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first(strtolower($criteria->code)) }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-save"></i> Simpan
                        </button>
                        <a href="{{route('alternative')}}" class="btn btn-sm btn-danger">
                            <i class="fa fa-arrow-left"></i> Kembali
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
