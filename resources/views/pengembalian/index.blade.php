@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-align-justify"></i> Pengembalian

                    <div class="card-header-actions">
                    </div>
                </div>
                <div class="card-body">
                    <table id="tbl-book" class="table table-striped table-bordered datatable dataTable no-footer">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Judul Buku</th>
                            <th>Pengarang</th>
                            <th>Peminjam</th>
                            <th>NIM</th>
                            <th>Tanggal Peminjaman</th>
                            <th>Tanggal Pengembalian</th>
                            <th>Denda</th>
                            {{--<th></th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        jQuery(document).ready(function ($) {
            var table = $('#tbl-book').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{{ route('return') }}",
                    "type": "POST",
                },
                columns: [
                    {data: null, "orderable": false, searchable: "false",
//                        render: function (data, type, row, meta) {
//                            return meta.row + 1;
//                        }
                    },
                    {data: 'rent.book.title', name: 'rent.book.title'},
                    {data: 'rent.book.author', name: 'rent.book.author'},
                    {data: 'rent.student.name', name: 'rent.student.name'},
                    {data: 'rent.student.nim', name: 'rent.student.nim'},
                    {data: 'rent.date', searchable: "false",
                        render: function (data, type, row, meta) {
                            return moment(data, "YYYY-MM-DD").format("DD MMM YY");
                        }
                    },
                    {data: 'created_at', searchable: "false",
                        render: function (data, type, row, meta) {
                            return moment(data, "YYYY-MM-DD").format("DD MMM YY");
                        }
                    },
                    {data: 'denda', name: 'denda', searchable: 'false',
                        render: function (data, type, row, meta) {
                            return "Rp." + data;
                        }
                    },
//                    {
//                        data: null, searchable: "false",
//                        render: function (data, type, row, meta) {
//                            var group = '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">';
//                            group += '<a href="/return/detail/' + data.id + '" data-toggle="tooltip" title="Detail peminjam" ' +
//                                'class="btn btn-brand btn-vine" data-id="' + data.id + '">' +
//                                '<i class="icons cui-people"></i></a>';
//                            group += '</div>';
//                            return group;
//                        },
//                        "orderable": false,
//                    }
                ],
                preDrawCallback: function(settings,json) {
                    if (settings.jqXHR != null)
                        settings.jqXHR.abort();
                },
                drawCallback: function (settings) {
                    $('[data-toggle="tooltip"]').tooltip();
                },
                "aoColumnDefs": [ {
                    "aTargets": [0],
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        if (iCol === 0) {
                            $(nTd).html(iRow + 1);
                        }
                    }
                }],
            });
        });
    </script>
@endsection
