
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

// window.axios = require('axios');

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    // window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

    $(document).ready(function ($) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });
} else {
    console.error('CSRF token not found.');
}

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// let token = document.head.querySelector('meta[name="csrf-token"]');
//
// if (token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
// } else {
//     console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
// }

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });


/** Core UI JS */
require('@coreui/coreui');
require('datatables.net');
require('datatables.net-bs4');
require('select2/dist/js/select2.full');
require('bootstrap-datepicker');

window.moment = require('moment');
window.moment.locale('id');

window.resetForm = function(form) {
    form.find('.form-control').removeClass('is-invalid');
    form.find('.invalid-feedback').remove();
}

window.handleError = function(response, form) {
    if (response.readyState == 4) {
        if(response.status === 422 || response.status === 423){
            var errors = response.responseJSON.errors;
            $.each(errors, function(key, error){
                var item = form.find('input[name='+ key +']');
                item = (item.length > 0) ? item : form.find('select[name='+ key +']');
                item = (item.length > 0) ? item : form.find('textarea[name='+ key +']');
                item = (item.length > 0) ? item : form.find("input[name='"+ key +"[]']");
                item.addClass('is-invalid');
                item.parent().append('<span class="invalid-feedback">'+ error +'</span>');
            })
        }else if (response.responseJSON.status == false){
            form.find('#error_message').addClass('has-error');
            form.find('#error_message').append('<span class="invalid-feedback">'+ response.responseJSON.message +'</span>');
        }else {
            form.find('#error_message').addClass('has-error');
            form.find('#error_message').append('<span class="invalid-feedback">Whoops, looks like something went wrong.</span>');
        }
    } else if (response.readyState == 0) {
        form.find('#error_message').addClass('has-error');
        form.find('#error_message').append('<span class="invalid-feedback">Request failed, please check your connection</span>');
    }
}