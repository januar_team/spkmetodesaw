<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    // Book
    Route::match(['get', 'post'], '/criteria', 'CriteriaController@index')->name('criteria');
    Route::match(['get', 'post'], '/criteria/add', 'CriteriaController@add')->name('criteria.add');
    Route::match(['get', 'post'], '/criteria/edit/{id}', 'CriteriaController@edit')->name('criteria.edit');
    Route::post('/criteria/delete', 'CriteriaController@delete')->name('criteria.delete');
    Route::match(['get', 'post'], '/criteria/rating/{id}', 'CriteriaController@rating')->name('criteria.rating');
    Route::post('/criteria/rating/add', 'CriteriaController@rating_add')->name('criteria.rating.add');
    Route::post('/criteria/rating/edit', 'CriteriaController@rating_edit')->name('criteria.rating.edit');
    Route::post('/criteria/rating/delete', 'CriteriaController@rating_delete')->name('criteria.rating.delete');

    // Class
    Route::match(['get', 'post'], '/alternative', 'AlternativeController@index')->name('alternative');
    Route::post('/alternative/add', 'AlternativeController@add')->name('alternative.add');
    Route::post('/alternative/edit', 'AlternativeController@edit')->name('alternative.edit');
    Route::post('/alternative/delete', 'AlternativeController@delete')->name('alternative.delete');
    Route::match(['get', 'post'], '/alternative/rating/{id}', 'AlternativeController@rating')->name('alternative.rating');

    Route::get('/processing', 'ProcessingController@index')->name('processing');

    // Student
    /*Route::match(['get', 'post'], '/student', 'StudentController@index')->name('student');
    Route::post('/student/add', 'StudentController@add')->name('student.add');
    Route::post('/student/edit', 'StudentController@edit')->name('student.edit');
    Route::post('/student/delete', 'StudentController@delete')->name('student.delete');
    Route::get('/student/search', 'StudentController@search')->name('student.search');
    Route::get('/student/get/{id}', 'StudentController@get')->name('student.get');*/

    // Peminjaman
    /*Route::match(['get', 'post'], '/rent', 'PeminjamanController@index')->name('rent');
    Route::match(['get', 'post'], '/rent/add', 'PeminjamanController@add')->name('rent.add');
    Route::get('/rent/detail/{id}', 'PeminjamanController@detail')->name('rent.detail');
    Route::post('/rent/delete', 'PeminjamanController@delete')->name('rent.delete');*/

    // Pengembalian
    /*Route::match(['get', 'post'], '/return', 'PengembalianController@index')->name('return');
    Route::match(['get', 'post'], '/return/add/{id}', 'PengembalianController@add')->name('return.add');*/
});
